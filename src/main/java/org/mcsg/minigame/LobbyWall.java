package org.mcsg.minigame;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;



public class LobbyWall {

	private ArrayList < Sign > signs = new ArrayList < Sign > ();
	private HashMap <Integer, String[]> text = new HashMap<Integer, String[]>();

	public LobbyWall() {

	}

	public boolean loadSign(World w, int x1, int x2, int z1, int z2, int y1) {
		boolean usingx = (x1 == x2) ? false : true;
		SkyWars.debug(w + " " + x1 + " " + x2 + " " + z1 + " " + z2 + " " + y1 + " " + usingx);
		int dir = new Location(w, x1, y1, z1).getBlock().getData();
		if (usingx) {
			for (int a = Math.max(x1, x2); a >= Math.min(x1, x2); a--) {
				Location l = new Location(w, a, y1, z1);
				BlockState b = l.getBlock().getState();
				if (b instanceof Sign) {
					signs.add((Sign) b);
					LobbySignManager.lobbychunks.add(b.getChunk());
					SkyWars.debug("usingx - " + b.getLocation().toString());
				} else {
					SkyWars.debug("Not a sign" + b.getType().toString());
					return false;
				}
			}
		} else {
			for (int a = Math.min(z1, z2); a <= Math.max(z1, z2); a++) {
				SkyWars.debug(a);
				Location l = new Location(w, x1, y1, a);
				BlockState b = l.getBlock().getState();
				if (b instanceof Sign) {
					signs.add((Sign) b);
					LobbySignManager.lobbychunks.add(b.getChunk());
					SkyWars.debug("notx - " + b.getLocation().toString());
				} else {
					SkyWars.debug("Not a sign" + b.getType().toString());
					return false;
				}
			}
		}
		SkyWars.debug("dir: " + dir);
		if (dir == 3 || dir == 5) {
			Collections.reverse(signs);
		}

		update();
		return true;
	}

	public int getSigns(){
		return signs.size();
	}

	public void setText(int sign, String ... texts){
		String[] lines = new String[4];

		for(int a = 0; a < 4; a++){
			if(texts.length <= a){
				lines[a] = "";
			}
			else{
				lines[a] = texts[a];
			}
		}
		text.put(sign, lines);
	}

	public void setLine(int sign, int line, String txt){
		String[] lines = text.get(sign);

		if(lines == null){
			lines = new String[4];
		}

		lines[line] = txt;

		text.put(sign, lines);

	}


	public void update() {
		int a = 0;

		boolean update = false;

		for(Sign s: signs){
			String[] lines = text.get(a);
			if(lines == null){
				lines = new String[4];
			}
			for(int b = 0; b < 4; b++){
				if(lines.length > b && lines[b] != null){
					if(!s.getLine(b).equals(lines[b])){
						s.setLine(b, lines[b]);
						update = true;
					}
				}
				else{
					if(!s.getLine(b).equals("")){
						s.setLine(b, "");
						update = true;
					}
				}
			}
			a++;
			if(update) s.update();
		}
	}


	public void clear() {
		for (Sign s: signs) {
			for (int a = 0; a < 4; a++) {
				s.setLine(a, "");
			}
			s.update();
		}
	}

	public void clearText(){
		text.clear();
	}










}