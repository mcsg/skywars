package org.mcsg.minigame.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_6_R2.CraftChunk;
import org.bukkit.inventory.ItemStack;
import org.mcsg.minigame.GameManager;
import org.mcsg.minigame.SkyWars;

public class SchematicLoader {

	private static SchematicLoader instance = new SchematicLoader();

	private HashMap<String, ArrayList<BlockData>> schem_cache = new HashMap<String, ArrayList<BlockData>>();
	private HashMap<String, ArrayList<  Point  >> spawn_cache = new HashMap<String, ArrayList<  Point  >>();
	private boolean useNMS = false;


	private SchematicLoader(){
		/*try {
			Class.forName("net.minecraft.server.v1_6_R2.Chunk");
			useNMS = true;
		} catch (ClassNotFoundException e) {
			System.out.println("---------------------\nSkyWars Warning: THE WORLD IS EXPLODING !@#OJ!O@J#:!O@J#!@# CLASSNOTFOUNDEXCEPTION OMGOMGOGMOMGMG\n\n\n-------------\n NMS class not found! Falling back to bukkit world loader\n-------------------");
			useNMS = false;
		}*/
	}

	public static SchematicLoader getInstance(){
		return instance;
	}

	
	public void clearCache(){
		schem_cache.clear();
		spawn_cache.clear();
	}
	
	public void remove(String schem){
		File folder = new File(GameManager.getInstance().getPlugin().getDataFolder(), "schematics");
		File scheme = new File(folder.getAbsolutePath(), schem);
		
		scheme.delete();
		schem_cache.remove(scheme);
		spawn_cache.remove(scheme);
	}

	public void load(String schematic, int xoff, int zoff){
		if(!schem_cache.containsKey(schematic)){
			loadSchematic(schematic);
		}
		if(useNMS)
			Bukkit.getScheduler().scheduleSyncDelayedTask(GameManager.getInstance().getPlugin(),
					new Loader2(new ArrayList<BlockData>(schem_cache.get(schematic)), xoff, zoff, calcPerTick(schematic)), 1);
		else
			Bukkit.getScheduler().scheduleSyncDelayedTask(GameManager.getInstance().getPlugin(),
					new Loader(new ArrayList<BlockData>(schem_cache.get(schematic)), xoff, zoff, calcPerTick(schematic)), 1);
	}



	public void loadSchematic(String schematic){
		try{
			File folder = new File(GameManager.getInstance().getPlugin().getDataFolder(), "schematics");
			if(!folder.exists()) folder.mkdir();
			File f = new File(folder, schematic);
			DataInputStream in = new DataInputStream(new BufferedInputStream(new FileInputStream(f)));

			int schem_size = in.readInt();

			ArrayList<BlockData>blocks = new ArrayList<BlockData>();
			ArrayList<  Point  >spawns = new ArrayList<  Point  >();


			for(int a = 0; a < schem_size; a++){
				BlockData b = new BlockData(in.readInt(), in.readByte(), 
						in.readInt(), in.readInt(), in.readInt());



				if(b.getid() == 19){
					spawns.add(new Point(b.getX(), b.getY(), b.getZ()));
				}else{
					blocks.add(b);
				}
			}

			schem_cache.put(schematic, blocks);
			spawn_cache.put(schematic, spawns);



			in.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	public void saveSchematic(ArrayList<BlockData> blocks, String schematic){
		try{
			File folder = new File(GameManager.getInstance().getPlugin().getDataFolder(), "schematics");
			if(!folder.exists()) folder.mkdir();
			File f = new File(folder, schematic);

			DataOutputStream out = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(f)));

			out.writeInt(blocks.size());
			for(BlockData b: blocks){
				//System.out.println("writing block "+b.toString());
				out.writeInt(b.getid());
				out.writeByte(b.getData());
				out.writeInt(b.getX());
				out.writeInt(b.getY());
				out.writeInt(b.getZ());
			}
			out.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}



	private int calcPerTick(String schem){
		return schem_cache.get(schem).size() / (45 * 20);
	}

	public ArrayList<Point> getSpawns(String schematic){
		if(!spawn_cache.containsKey(schematic)){
			loadSchematic(schematic);
		}
		return spawn_cache.get(schematic);
	}


	class Loader implements Runnable{

		int xoff, zoff, pertick, i, a;
		ArrayList<BlockData>blocks;

		Random r = new Random();

		public Loader(ArrayList<BlockData> blocks, int xoff, int zoff, int pertick){
			this(blocks, xoff, zoff, pertick, blocks.size() - 1);
		}

		public Loader(ArrayList<BlockData> blocks, int xoff, int zoff, int pertick, int i){
			this.blocks = blocks;
			this.xoff = xoff;
			this.zoff = zoff;
			this.pertick = pertick;
			this.i = i;
		}

		public void run() {
			while(i>=0 && a < pertick){
				//				BlockData result = blocks.remove(r.nextInt(blocks.size()));
				BlockData result = blocks.get(i);

				Location l = new Location(Bukkit.getWorld(SkyWars.WORLD_NAME), 
						result.getX() + xoff, result.getY(), result.getZ() + zoff);

				if(result.getid() == 35 && l.clone().add(0,-1,0).getBlock().getState() instanceof Chest){
					Chest c = (Chest)l.clone().add(0,-1,0).getBlock().getState();
					c.getInventory().setItem(0, new ItemStack(35,1, result.getData()));
				}
				else{
					Block b = l.getBlock();
					b.setTypeIdAndData(result.getid(), result.getData(), false);
					b.getState().update();
				}

				i--;
				a++;
			}
			if(i != -1){
				Bukkit.getScheduler().scheduleSyncDelayedTask(GameManager.getInstance().getPlugin(),
						new Loader(blocks, xoff, zoff, pertick, i), 1);
			}
		}
	}

	class Loader2 implements Runnable{

		int xoff, zoff, pertick, i, a;
		ArrayList<BlockData>blocks;

		Random r = new Random();

		public Loader2(ArrayList<BlockData> blocks, int xoff, int zoff, int pertick){
			this(blocks, xoff, zoff, pertick, blocks.size() - 1);
		}

		public Loader2(ArrayList<BlockData> blocks, int xoff, int zoff, int pertick, int i){
			this.blocks = blocks;
			this.xoff = xoff;
			this.zoff = zoff;
			this.pertick = pertick;
			this.i = i;
		}

		public void run() {
			long time = System.currentTimeMillis();
			while(i>=0 && a < pertick){
				//				BlockData result = blocks.remove(r.nextInt(blocks.size()));
				BlockData result = blocks.get(i);

				Location l = new Location(Bukkit.getWorld(SkyWars.WORLD_NAME), 
						result.getX() + xoff, result.getY(), result.getZ() + zoff);

				if(result.getid() == 35 && l.clone().add(0,-1,0).getBlock().getState() instanceof Chest){
					Chest c = (Chest)l.clone().add(0,-1,0).getBlock().getState();
					c.getInventory().setItem(0, new ItemStack(35,1, result.getData()));
				}
				else{
					net.minecraft.server.v1_6_R2.Chunk c = ((CraftChunk)l.getChunk()).getHandle();
					c.a(result.getX() & 15, result.getY(), result.getZ() & 15, result.getid(), result.getData());
				}

				i--;
				a++;
			}
			if(i != -1){
				Bukkit.getScheduler().scheduleSyncDelayedTask(GameManager.getInstance().getPlugin(),
						new Loader2(blocks, xoff, zoff, pertick, i), 1);
			}
			//System.out.println("Time: "+(System.currentTimeMillis() - time)+" Blocks: "+blocks.size());
		}
	}


}
