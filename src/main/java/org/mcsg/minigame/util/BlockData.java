package org.mcsg.minigame.util;

public class BlockData{



    private byte data;
    private int id,x,y,z;
    
    /**
*
* @param previd
* @param newid
* @param x
* @param y
* @param z
*
* Provides a object for holding the data for block changes
*/
    public BlockData(int id,byte data, int x, int y, int z){
        this.id = id;
    	this.data = data;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    

    public byte getData() {
        return data;
    }

    public int getid() {
        return id;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    public String toString(){
    	return "["+data+"]"+"["+id+"]"+"["+x+"]"+"["+y+"]"+"["+z+"]";
    }
    
    
}