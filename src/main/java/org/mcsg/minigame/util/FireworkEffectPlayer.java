package org.mcsg.minigame.util;

import java.lang.reflect.Method;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkEffectPlayer {
	
	/*
	 * Example use:
	 * 
	 * public class FireWorkPlugin implements Listener {
	 * 
	 * FireworkEffectPlayer fplayer = new FireworkEffectPlayer();
	 * 
	 * @EventHandler
	 * public void onPlayerLogin(PlayerLoginEvent event) {
	 *   fplayer.playFirework(event.getPlayer().getWorld(), event.getPlayer.getLocation(), Util.getRandomFireworkEffect());
	 * }
	 * 
	 * }
	 */

    // internal references, performance improvements
    private Method world_getHandle = null;
    private Method nms_world_broadcastEntityEffect = null;
    private Method firework_getHandle = null;

    /**
     * Play a pretty firework at the location with the FireworkEffect when called
     * @param world
     * @param loc
     * @param fe
     * @throws Exception
     */
    public void playFirework(World world, Location loc, FireworkEffect fe) throws Exception {
        // Bukkity load (CraftFirework)
        Firework fw = (Firework) world.spawn(loc, Firework.class);
        // the net.minecraft.server.World
        Object nms_world = null;
        Object nms_firework = null;
		/*
		 * The reflection part, this gives us access to funky ways of messing around with things
		 */
        if(world_getHandle == null) {
            // get the methods of the craftbukkit objects
            world_getHandle = getMethod(world.getClass(), "getHandle");
            firework_getHandle = getMethod(fw.getClass(), "getHandle");
        }
        // invoke with no arguments
        nms_world = world_getHandle.invoke(world, (Object[]) null);
        nms_firework = firework_getHandle.invoke(fw, (Object[]) null);
        // null checks are fast, so having this seperate is ok
        if(nms_world_broadcastEntityEffect == null) {
            // get the method of the nms_world
            nms_world_broadcastEntityEffect = getMethod(nms_world.getClass(), "broadcastEntityEffect");
        }
		/*
		 * Now we mess with the metadata, allowing nice clean spawning of a pretty firework (look, pretty lights!)
		 */
        // metadata load
        FireworkMeta data = (FireworkMeta) fw.getFireworkMeta();
        // clear existing
        data.clearEffects();
        // power of one
        data.setPower(1);
        // add the effect
        data.addEffect(fe);
        // set the meta
        fw.setFireworkMeta(data);
		/*
		 * Finally, we broadcast the entity effect then kill our fireworks object
		 */
        // invoke with arguments
        nms_world_broadcastEntityEffect.invoke(nms_world, new Object[] {nms_firework, (byte) 17});
        // remove from the game
        fw.remove();
    }

    /**
     * Internal method, used as shorthand to grab our method in a nice friendly manner
     * @param cl
     * @param method
     * @return Method (or null)
     */
    private static Method getMethod(Class<?> cl, String method) {
        for(Method m : cl.getMethods()) {
            if(m.getName().equals(method)) {
                return m;
            }
        }
        return null;
    }
    
    public static FireworkEffect getRandomEffect()
	{
		Random random = new Random();
		int picked = random.nextInt(5);
		switch(picked)
		{
			case 0:
			{
				FireworkEffect effect = (FireworkEffect.builder().trail(true).withColor(Color.RED).withColor(Color.FUCHSIA).withFade(Color.MAROON).flicker(false).with(FireworkEffect.Type.CREEPER)).build();
				return effect;
			}
			case 1:
			{
				FireworkEffect effect = (FireworkEffect.builder().trail(false).withColor(Color.ORANGE).withColor(Color.RED).withFade(Color.NAVY).flicker(true).with(FireworkEffect.Type.BALL_LARGE)).build();
				return effect;
			}
			case 2:
			{
				FireworkEffect effect = (FireworkEffect.builder().trail(true).withColor(Color.TEAL).withColor(Color.PURPLE).withFade(Color.OLIVE).flicker(false).with(FireworkEffect.Type.BURST)).build();
				return effect;
			}
			case 3:
			{
				FireworkEffect effect = (FireworkEffect.builder().trail(false).withColor(Color.PURPLE).withColor(Color.LIME).withFade(Color.BLUE).flicker(true).with(FireworkEffect.Type.STAR)).build();
				return effect;
			}
			case 4:
			{
				FireworkEffect effect = (FireworkEffect.builder().trail(true).withColor(Color.LIME).withColor(Color.PURPLE).withFade(Color.RED).flicker(false).with(FireworkEffect.Type.CREEPER)).build();
				return effect;
			}
			case 5:
			{
				FireworkEffect effect = (FireworkEffect.builder().trail(false).withColor(Color.AQUA).withColor(Color.GREEN).withFade(Color.BLUE).flicker(true).with(FireworkEffect.Type.BURST)).build();
				return effect;
			}
		}
		FireworkEffect effect = (FireworkEffect.builder().trail(false).withColor(Color.AQUA).withColor(Color.GREEN).withFade(Color.BLUE).flicker(true).with(FireworkEffect.Type.BURST)).build();
		return effect;
        
	}

}