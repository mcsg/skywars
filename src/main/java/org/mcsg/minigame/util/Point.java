package org.mcsg.minigame.util;

public class Point {

	private double x = 0;
	private double y = 0;
	private double z = 0;
	
	public Point(double x, double y, double z){
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public Point(){
		
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return z;
	}

	public void setZ(double z) {
		this.z = z;
	}
	
	public Point add(double x, double y, double z){
		this.x += x;
		this.y += y;
		this.z += z;
		
		return this;
	}
	
	public Point clone(){
		return new Point(x,y,z);
	}
	
}
