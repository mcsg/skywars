package org.mcsg.minigame.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.mcsg.minigame.SettingsManager;



public class ChestRatioStorage {

	public static enum ChestLevel { LOW, MEDIUM, HIGH,  OP };

	HashMap<ChestLevel,  ArrayList<ItemStack>>lvlstore = new HashMap<ChestLevel, ArrayList<ItemStack>>();
	public static ChestRatioStorage instance = new ChestRatioStorage();
	int ratio = 2;

	private ChestRatioStorage(){ }

	public ChestLevel getChestLevel(Location chest) {
		Block b = chest.getBlock();
		if (b != null && b.getState() instanceof Chest) {
			Chest c = (Chest)b.getState();
			Inventory i = c.getInventory();

			ItemStack wool = i.getItem(0);
			if (wool != null && wool.getType() == Material.WOOL) {
				return getLevel(wool.getData().getData());
			}
		}
		return ChestLevel.LOW;
	}
	private ChestLevel getLevel(byte data) {
		switch(data) {
		case 1://Orange
		case 2://Magenta
		case 3://Light Blue
		case 4://Yellow
			return ChestLevel.LOW;
		case 5://Light green
		case 6://Pink
		case 7://Gray
		case 8://LightGray
			return ChestLevel.MEDIUM;
		case 9://Cyan
		case 10://Purple
		case 11://Blue
		case 12://Brown
			return ChestLevel.HIGH;
		case 13://DarkGreen
		case 14://Red
		case 15://Black
			return ChestLevel.OP;

		}
		return ChestLevel.LOW;
	}

	public void setup(){

		FileConfiguration conf = SettingsManager.getInstance().getChest();

		for(ChestLevel clevel: ChestLevel.values()){
			ArrayList<ItemStack> lvl = new ArrayList<ItemStack>();
			List<String>list = conf.getStringList("chest."+clevel);

			for(int b = 0; b<list.size();b++){
				ItemStack i = ItemReader.read(list.get(b));
				lvl.add(i);
			}
			lvlstore.put(clevel, lvl);
		}
	}

	public static ChestRatioStorage getInstance(){
		return instance;
	}

	public ArrayList<ItemStack> getItems(ChestLevel level){
		Random r = new Random();
		ArrayList<ItemStack>items = new ArrayList<ItemStack>();
		//TODO: Implement some sort of actual system
		//Leaving that to double, since he seems to know exactly how he wants it

		for(int a = 0; a< r.nextInt(7)+15; a++){
			if(r.nextBoolean() == true){

				ArrayList<ItemStack>lvl = lvlstore.get(level);
				ItemStack item = lvl.get(r.nextInt(lvl.size()));

				items.add(item);

			}

		}
		return items;
	}
}