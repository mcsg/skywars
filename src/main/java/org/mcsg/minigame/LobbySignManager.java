package org.mcsg.minigame;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.mcsg.minigame.Game.GameStatus;

public class LobbySignManager implements Listener{

	private static LobbySignManager instance = new LobbySignManager();
	public static ArrayList<Chunk>lobbychunks = new ArrayList<Chunk>();

	private HashMap<Integer, LobbyWall> wall = new HashMap<Integer, LobbyWall>();
	private int topGame = 0;
	private boolean setup = false;

	private LobbySignManager(){

	}


	public static LobbySignManager getInstance(){
		return instance;
	}

	@EventHandler
	public void keepLobbyLoaded(ChunkUnloadEvent e){
		if(lobbychunks.contains(e.getChunk())){
			e.setCancelled(true);
		}
	}



	public void setup(){
		Bukkit.getServer().getPluginManager().registerEvents(this, GameManager.getInstance().getPlugin());
		FileConfiguration s = SettingsManager.getInstance().getConfig();
		String world = s.getString("signs.world");
		if(world == null) return;
		World w = Bukkit.getWorld(world);
		int x1 = s.getInt("signs.x1");
		int y1 = s.getInt("signs.y1");
		int z1 = s.getInt("signs.z1");
		int x2 = s.getInt("signs.x2");
		int y2 = s.getInt("signs.y2");
		int z2 = s.getInt("signs.z2");

		int b = 0;
		for(int a = y1; a > y2; a--){
			System.out.println("Loading "+a);
			LobbyWall sign = new LobbyWall();
			System.out.println(sign.loadSign(w, x1, x2, z1, z2, a));

			wall.put(b, sign);
			b++;
		}
		setup = true;
	}


	int blank = 2;
	int cycle = 0;
	int subcycle = 14;

	public StringBuilder getSub(){
		StringBuilder sb = new StringBuilder();
		for(int a = 0; a< 15; a++){
			sb.append((a == subcycle)?"  ":"=");
		}
		return sb;
	}


	public void update(){
		if(!setup) return;
		List<Game>games = new ArrayList<Game>(GameManager.getInstance().getGames());


		if(topGame >= games.size()){
			topGame = 0;
		} else if (wall.size() - 1 < games.size()){
			for(int a = 0; a < topGame; a++){
				games.add(games.remove(0));
			}
		}

		cycle++;
		if(cycle == 60){
			topGame++;
			cycle = 0;
		}


		LobbyWall s1 = wall.get(0);
		int center = s1.getSigns()/2;
		s1.clearText();
		StringBuilder sub = getSub();
		for(int a = 0; a < s1.getSigns(); a++){
			if(a != center + blank && a != center - blank)
				s1.setText(a, "","================","===================","");
			else{
				s1.setText(a, "",sub.toString(),sub.toString(),"");
				sub.reverse();
			}
		}

		if(subcycle == 0){
			blank = (blank > s1.getSigns() / 2 )? 2 : blank+1;
			subcycle = 14;
		} 
		else{
			subcycle--;
		}

		s1.setText(center, ChatColor.DARK_RED+"[SkyWars]", "play.mc-sg.org", "Double0negative","Dread9nought");
		s1.setText(center-1, "",ChatColor.BLUE+"Games Running",ChatColor.DARK_BLUE+""+games.size());
		s1.setText(center+1, "",ChatColor.BLUE+"Players ", ChatColor.DARK_BLUE+""+GameManager.getInstance().getTotalPlayers());
		s1.update();

		for(int a = 1; a < wall.size(); a++){
			LobbyWall s = wall.get(a);
			s.clearText();

			if(a > games.size()){
				s.clear();

				continue;
			}
			Game game = games.get(a-1);
			if(game == null){

				continue;
			}
			if(game.getStatus() != GameStatus.FINISHED){
				if(game.getStatus() == GameStatus.WAITING)
					s.setText(center, ChatColor.DARK_RED+"[SkyWars]", 
							ChatColor.DARK_BLUE+"[Game "+game.getID()+"]", 
							"WAITING", ""+((game.getCountdownTask() != null)?  game.getCountdownTask().getTime(): ""));
				else 
					s.setText(center, ChatColor.DARK_RED+"[SkyWars]", ChatColor.DARK_BLUE+"[Game "+game.getID()+"]", "INGAME");


				s.setText(center - 1, "",ChatColor.BLUE+"Map", ChatColor.DARK_BLUE+game.getMap().getName());
				s.setText(center + 1, "",ChatColor.BLUE+"Map", ChatColor.DARK_BLUE+game.getMap().getName());

				int no = 2;
				int line = 0;
				for(Player p: game.getPlayers()){
					String n = p.getName();
					String name = (SkyWars.isDev(n) ? ChatColor.DARK_BLUE + ((n.equalsIgnoreCase("double0negative")) ? "Double0" : (n.substring(0, Math.min(n.length(), 14)))) : n);
					s.setLine(center + (no     ), line , name);
					s.setLine(center + (no * -1), line , name);
					if(++line >= 4){
						no++;
						line = 0;
					}
				}

			} else {
				s.setText(center, ChatColor.RED+"[SkyWars]", ChatColor.BLUE+"[Game "+game.getID()+"]", "FINISHED");

				s.setText(center - 1, "", ChatColor.BLUE+"WINNER",game.getWinner());
				s.setText(center + 1, "", ChatColor.BLUE+"WINNER",game.getWinner());

			}
			s.update();
		}
	}
}
