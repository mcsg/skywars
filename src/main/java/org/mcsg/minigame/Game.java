package org.mcsg.minigame;

import java.util.ArrayList;


import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.mcsg.minigame.MessageManager.PrefixType;
import org.mcsg.minigame.Tasks.CountdownTask;
import org.mcsg.minigame.util.Point;

public class Game
{
	//Static stuff
	private static int lastID = 0;

	public static int getGameID() {
		return ++lastID;
	}
	public static enum GameStatus { WAITING, INGAME, FINISHED }

	//Dynamic stuff

	//Variables
	private GameStatus gameStatus = GameStatus.WAITING;
	private ArrayList<Player> gamePlayers = new ArrayList<Player>();
	private Map map;
	private int gameID;
	private int minPlayers = 2;

	private int blocksBroken = 0;
	private int blocksPlaced = 0;

	private long startTime;
	private String winner;
	private Scoreboard scoreboard;
	//Tasks
	private CountdownTask cTask;



	public Game(Map map, int gameid)
	{
		this.gameID = gameid;
		this.map = map;
		this.scoreboard = new Scoreboard(this);

	}

	//Core methods
	public void addPlayer(Player p) {
		p.setGameMode(GameMode.SURVIVAL);
		p.setFlying(false);
		p.setAllowFlight(false);
		p.setNoDamageTicks(30);
		hardAddPlayer(p);
		InventoryManager.store(p);

		if (getCountdownTask() == null) {
			map.createSpawnPoints();

			startNewCountdown();
			map.load();
		}

		p.teleport(getLocation(map.getNextSpawn()));
		if (SkyWars.isDev(p.getName()))
			msgFall("game.devjoin", "player-"+p.getName());
		else
			msgFall("game.join","player-"+p.getName(), "max-"+map.getMaxSpawns(), "players-"+gamePlayers.size());

		getScoreboard().Updatescoreboard();
	}

	public void startGame() {
		if (getStatus() == GameStatus.WAITING) {
			if (getPlayerCount() >= getMinPlayers()) {
				map.destroySpawnPoints();
				msgFall("game.starting");
				setStatus(GameStatus.INGAME);
				startTime = System.currentTimeMillis();
			}
			else {
				msgFall("game.nostart");
				for(Player p : gamePlayers.toArray(new Player[gamePlayers.size()]))
					removePlayer(p);
				GameManager.getInstance().removeGame(this);
			}
		}
	}

	public void Killed(Player killed, Player killer) {
		msgFall("death.killed","killed-"+killed.getName(), "killer-"+killer.getName());
		removePlayer(killed);
		if (getPlayerCount() == 1) {
			win();
		}
	}


	public void Killed(Player killed, DamageCause cause) {
		String killer = "";
		Player kp = DamageTracker.getLastDamager(killed);
		if (kp != null) {
			killer = kp.getName();
			DamageTracker.destroyChip(killed);
		}
		switch(cause)
		{
			case VOID:
			{
				if (killer != "") {
					msgFall("death.voidp", "killed-"+killed.getName(), "killer-"+killer);
				}
				else {
					msgFall("death.void", "player-"+killed.getName());
				}
				break;
			}
			case PROJECTILE:
			{
				if (killer != "") {
					msgFall("death.projectile", "killed-"+killed.getName(), "killer-"+killer);
				}
				break;
			}
			case FIRE:
			case FIRE_TICK:
			case LAVA:
			{
				msgFall("death.fire", "player-"+killed.getName());
				break;
			}
			case FALL:
			{
				msgFall("death.fall", "player-"+killed.getName());
				break;
			}
			default:
			{
				msgFall("death.unknown", "player-"+killed.getName(), "cause-", cause.toString());
				break;
			}
				
		}
		
		removePlayer(killed);
		killed.setHealth(killed.getMaxHealth());
		killed.setFoodLevel(20);
		InventoryManager.restore(killed);
		if (getPlayerCount() == 1) {
			win();
		}
	}


	public void win()
	{
		getScoreboard().Removescoreboards();
		Player winner = gamePlayers.get(0);
		removePlayer(winner);
		InventoryManager.restore(winner);
		broadcast("game.win","player-"+winner.getName(), "map-"+map.getName());
		winner.setHealth(winner.getMaxHealth());
		this.winner = winner.getName();
		gameStatus = GameStatus.FINISHED;
		delayRemoveGame();
	}
	public String getWinner(){
		return this.winner;
	}
	//delay the remove of this game for sign purposes
	public void delayRemoveGame(){
		final Game game = this;
		Bukkit.getScheduler().scheduleSyncDelayedTask(GameManager.getInstance().getPlugin(), new Runnable(){
			public void run(){
				GameManager.getInstance().removeGame(game);
			}
		}, 600);
	}
	public void removePlayer(final Player p) {
		if (containsPlayer(p)) {
			hardRemovePlayer(p);
			getScoreboard().Updatescoreboard();
			p.setScoreboard(getScoreboard().getManager().getNewScoreboard());//getNew returns a blank
			p.teleport(SettingsManager.getInstance().getLobbySpawn());
			
			if (getStatus() == GameStatus.INGAME && getPlayerCount() > 1) {
				msgFall("game.playersleft", "playersalive-"+getPlayerCount());
			}
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(SkyWars.getPlugin(), new Runnable()
			{
				Player x = p;
				public void run()
				{
					if (x.getWorld().getName().equalsIgnoreCase(SkyWars.WORLD_NAME))
						x.teleport(SettingsManager.getInstance().getLobbySpawn());
				}
			}, 6L);
		}
	}
	public void playerLeave(Player p) {
		msgFall("game.leave", "player-"+p.getName());
		removePlayer(p);
	}
	public void setAllEXP(int level) {
		for (Player p : gamePlayers) {
			p.setLevel(level);
		}
	}

	// Boring Get & Set accessors
	public int getID() {
		return gameID;
	}
	public Map getMap() {
		return map;
	}
	public GameStatus getStatus() {
		return gameStatus;
	}
	public void setStatus(GameStatus status) {
		gameStatus = status;
	}
	public Integer getMaxPlayers() {
		return map.getMaxSpawns();
	}
	public Integer getMinPlayers() {
		return minPlayers;
	}
	public Integer getPlayerCount() {
		return gamePlayers.size();
	}
	public Integer getBlocksBroken() {
		return blocksBroken;
	}
	public void addBlockBroken() {
		blocksBroken++;
		getScoreboard().Updatescoreboard();
	}
	public Integer getBlocksPlaced() {
		return blocksPlaced;
	}
	public void addBlockPlaced() {
		blocksPlaced++;
		getScoreboard().Updatescoreboard();
	}
	public ArrayList<Player> getPlayers() {
		return gamePlayers;
	}
	public Scoreboard getScoreboard() {
		return scoreboard;
	}
	public CountdownTask getCountdownTask() {
		return cTask;
	}
	public void startNewCountdown() {
		cTask = new CountdownTask(this);
		cTask.start();
	}

	public boolean full() {
		return getPlayerCount() >= map.getMaxSpawns() ;
	}

	public boolean spawnsFull(){
		return map.getMaxSpawns() <= map.getCurrentSpawn()+1;
	}

	//Player methods
	public boolean containsPlayer(Player p) {
		return gamePlayers.contains(p);
	}
	public boolean containsPlayer(String name) {
		for (Player p : gamePlayers) {
			if (p.getName().equalsIgnoreCase(name))
				return true;
		}
		return false;
	}
	private void hardAddPlayer(Player p) {
		if (containsPlayer(p)) {
			hardRemovePlayer(p);
		}

		gamePlayers.add(p);
	}
	private void hardRemovePlayer(Player p) {

		if (containsPlayer(p)) {
			gamePlayers.remove(p);
		}
	}

	public void msgall(String message){
		for(Player p: gamePlayers){
			MessageManager.getInstance().sendMessage(PrefixType.INFO, message,  p);
		}
	}

	public void msgFall(String msg, String ... args){
		for(Player p: gamePlayers){
			MessageManager.getInstance().sendFMessage(PrefixType.INFO, msg, p, args);
		}
	}
	public void broadcast(String msg, String ... args){
		MessageManager.getInstance().broadcastFMessage(PrefixType.INFO, msg, args);
	}
	//Aux methods
	public Location getLocation(Point p){
		World w = Bukkit.getWorld(SkyWars.WORLD_NAME);
		return new Location(w, p.getX(), p.getY(), p.getZ());

	}

	public boolean isGracePeriod(){
		return (System.currentTimeMillis() < startTime + 7000) || gameStatus == GameStatus.WAITING;
	}


}
