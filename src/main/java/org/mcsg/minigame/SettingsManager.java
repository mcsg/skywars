package org.mcsg.minigame;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;


public class SettingsManager {

	//makes the config easily accessible

	private static SettingsManager instance = new SettingsManager();
	private static Plugin p;

	private FileConfiguration kits;
	private FileConfiguration messages;
	private FileConfiguration chest;


	private File f1; //spawns
	private File f2; //system
	private File f3; //kits

	private static final int KIT_VERSION = 0;
	private static final int MESSAGE_VERSION = 0;
	private static final int CHEST_VERSION = 0;


	private SettingsManager() {

	}

	public static SettingsManager getInstance() {
		return instance;
	}

	public void setup(Plugin p) {
		SettingsManager.p = p;
	/*	if (p.getConfig().getInt("config-version") == Assassins.config_version) {
			Assassins.config_todate = true;
		}else{
			File config = new File(p.getDataFolder(), "config.yml");
			config.delete();
		}*/

		p.getConfig().options().copyDefaults(true);
		p.saveDefaultConfig();


		f1 = new File(p.getDataFolder(), "kits.yml");
		f2 = new File(p.getDataFolder(), "messages.yml");
		f3 = new File(p.getDataFolder(), "chest.yml");

		try {
			if (!f1.exists()) 	loadFile("kits.yml");
			if (!f2.exists()) 	loadFile("messages.yml");
			if (!f3.exists()) 	loadFile("chest.yml");

		} 
		catch (Exception e) {
			e.printStackTrace();
		}


		reloadKits();
		//saveKits();

		reloadChest();

		reloadMessages();
		saveMessages();


	}

	public void set(String arg0, Object arg1) {
		p.getConfig().set(arg0, arg1);
	}

	public FileConfiguration getConfig() {
		return p.getConfig();
	}


	public FileConfiguration getKits() {
		return kits;
	}

	public FileConfiguration getChest() {
		return chest;
	}

	public FileConfiguration getMessageConfig() {
		return messages;
	}


	public void saveConfig() {
		 p.saveConfig();
	}


	@SuppressWarnings("unused")
	public void reloadKits() {
		//Spammed my console
		if (10 > 1) { return; }
		kits = YamlConfiguration.loadConfiguration(f1);
		if(kits.getInt("version", 0) != KIT_VERSION){
			SkyWars.$("Deleting outdated config file. "+f1.getName());
			f2.delete();
			loadFile("kits.yml");
			reloadKits();
		}
	}


	public void reloadMessages() {
		messages = YamlConfiguration.loadConfiguration(f2);
		if(messages.getInt("version", 0) != MESSAGE_VERSION){
			SkyWars.$("Deleting outdated config file. "+f2.getName());
			f2.delete();
			loadFile("messages.yml");
			reloadKits();
		}
		messages.set("version", MESSAGE_VERSION);
		saveMessages();
	}

	public void reloadChest() {
		chest = YamlConfiguration.loadConfiguration(f3);
		if(chest.getInt("version", 0) != CHEST_VERSION){
			SkyWars.$("Deleting outdated config file. "+f3.getName());
			f3.delete();
			loadFile("chest.yml");
			reloadKits();
		}
	}


	public void saveKits() {
		try {
			kits.save(f1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveMessages() {
		try {
			messages.save(f2);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void saveChest() {
		try {
			chest.save(f3);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}



	public Location getLobbySpawn() {
		try{
			return new Location(Bukkit.getWorld(getConfig().getString("spawn.world")),
				getConfig().getInt("spawn.x"),
				getConfig().getInt("spawn.y"),
				getConfig().getInt("spawn.z"));
		} catch(Exception e){
			return null;
		}
	}
	public void setLobbySpawn(Location l) {
		getConfig().set("spawn.world", l.getWorld().getName());
		getConfig().set("spawn.x", l.getBlockX());
		getConfig().set("spawn.y", l.getBlockY());
		getConfig().set("spawn.z", l.getBlockZ());
		saveConfig();
	}


	public static String getSqlPrefix() {
		return getInstance().getConfig().getString("sql.prefix");
	}


	public void loadFile(String file){
		File t = new File(p.getDataFolder(), file);
		System.out.println("Writing new file: "+ t.getAbsolutePath());

			try {
				t.createNewFile();
				FileWriter out = new FileWriter(t);
				System.out.println(file);
				BufferedReader br = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/"+file)));
				String line;
				while ((line = br.readLine()) != null) {
					out.write(line+"\n");
					System.out.println(line);
				}
				out.flush();
				br.close();
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

	}
}