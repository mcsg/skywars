package org.mcsg.minigame;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcsg.minigame.util.ChestRatioStorage;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;

public class SkyWars extends JavaPlugin{

	public static Logger logger;
	public static final String WORLD_NAME = "skywars_gameworld";

	public static final ArrayList<Chunk> chunks = new ArrayList<Chunk>();
	public int chunkno = 0;

	private static SkyWars instance;

	public void onLoad(){
		deleteDirectory(new File(WORLD_NAME));
	}


	public void onEnable(){
		instance = this;
		logger = getLogger();
		SettingsManager.getInstance().setup(this);
		MessageManager.getInstance().setup();
		GameManager.getInstance().setup();
		ChestRatioStorage.getInstance().setup();
		Events.setup();
		getCommand("skywars").setExecutor(new CommandHandler(this));
		LobbySignManager.getInstance().setup();

		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			public void run(){
				LobbySignManager.getInstance().update();
			}
		}, 2, 1);


		getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable(){
			public void run(){
				if(chunks.size() == 0) return;
				for(int a = 0; a < 25; a++){
					if(chunkno >= chunks.size()){
						chunkno = 0;
					}
					chunks.get(chunkno).unload(false,  true);
					chunkno++;
				}

			}
		}, 0, 1);
		
		try {
			new Metrics(this).start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onDisable(){
		for(Player p: Bukkit.getOnlinePlayers()){
			if(p.getWorld().getName().equals(WORLD_NAME)){
				InventoryManager.restore(p);
				p.teleport(SettingsManager.getInstance().getLobbySpawn());
			}
		}
		Bukkit.unloadWorld(WORLD_NAME, false);
		deleteDirectory(new File(WORLD_NAME));

	}
	public static void $(String msg){
		logger.log(Level.INFO, msg);
	}

	public static void $(Level l, String msg){
		logger.log(l, msg);
	}

	public static void debug(String msg){
		if(SettingsManager.getInstance().getConfig().getBoolean("debug", false))
			$(msg);
	}

	public static void debug(int a) {
		if(SettingsManager.getInstance().getConfig().getBoolean("debug", false))
			debug(a+"");
	}

	public WorldEditPlugin getWorldEdit() {
		Plugin worldEdit = getServer().getPluginManager().getPlugin("WorldEdit");
		if (worldEdit instanceof WorldEditPlugin) {
			return (WorldEditPlugin) worldEdit;
		} else {
			return null;
		}
	}

	public static SkyWars getPlugin() {
		return instance;
	}

	public static boolean deleteDirectory(File directory) {
		if(directory.exists()){
			File[] files = directory.listFiles();
			if(null!=files){
				for(int i=0; i<files.length; i++) {
					if(files[i].isDirectory()) {
						deleteDirectory(files[i]);
					}
					else {
						files[i].delete();
					}
				}
			}
		}
		return(directory.delete());
	}
	public static boolean isDev(String name) {
		return name.equalsIgnoreCase("double0negative") || name.equalsIgnoreCase("dread9nought") || name.equalsIgnoreCase("imalo");
	}

}
