package org.mcsg.minigame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.mcsg.minigame.util.Point;
import org.mcsg.minigame.util.SchematicLoader;

public class Map {

	private String schematic;

	private int offx, offz;

	private int spawn = -1;

	private List<Point>spawn_cache;

	public Map(String schematic, int offx, int offz){
		this.schematic = schematic;
		this.offx = offx;
		this.offz = offz;
	}

	public void load(){
		SchematicLoader.getInstance().load(schematic, offx, offz);
	}

	public int getMaxSpawns(){
		return SchematicLoader.getInstance().getSpawns(schematic).size();
	}

	public int getCurrentSpawn(){
		return spawn;
	}


	public Point getNextSpawn(){
		spawn++;
		if(spawn_cache != null){
			return spawn_cache.get(spawn);
		}
		else{
			return SchematicLoader.getInstance().getSpawns(schematic).get(spawn).add(offx + 0.5, 0, offz + 0.5);
		}
	}

	public List<Point> getAllSpawns(){
		if(spawn_cache == null){
			List<Point> spawns = new ArrayList<Point>();
			for(Point s:SchematicLoader.getInstance().getSpawns(schematic)){
				spawns.add(s.clone().add(offx + 0.5, 0, offz + 0.5));
			}
			spawn_cache = spawns;
		}
		return spawn_cache;
	}

	public void createSpawnPoints(){
		for(Point p: getAllSpawns()){
			createBox(p, 20);
		}
	}

	public void destroySpawnPoints(){
		for(Point p: getAllSpawns()){
			createBox(p, 0);
		}
	}

	public String getName(){
		return schematic;
	}


	private void createBox(Point p, int id){
		double xcent = p.getX();
		double ycent = p.getY();
		double zcent = p.getZ();

		World w = Bukkit.getWorld(SkyWars.WORLD_NAME);

		for(double x = xcent - 1; x <= xcent + 1; x++){
			for(double y = ycent -1; y <= ycent + 2; y++){
				for(double z = zcent - 1; z <= zcent + 1; z++){
					Location l = new Location(w, x, y, z);
					Block b = l.getBlock();
					b.setTypeId(id);
					b.getState().update();
					l.getWorld().playEffect(l, Effect.MOBSPAWNER_FLAMES, 200);
				}
			}
		}

		Location l = new Location(w, xcent, ycent, zcent);
		Block b = l.getBlock();
		b.setTypeId(0);
		b.getState().update();

		l = l.add(0, 1, 0);
		b = l.getBlock();
		b.setTypeId(0);
		b.getState().update();
	}
}
