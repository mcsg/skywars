package org.mcsg.minigame.commands;

import org.mcsg.minigame.GameManager;
import org.bukkit.entity.Player;

public class Join implements SubCommand{
	@Override
	public boolean onCommand(Player player, String[] args) {
		if(!GameManager.getInstance().isPlayerActive(player)){
			GameManager.getInstance().addPlayer(player);
		}
		return true;

	}
	@Override
	public String help(Player p) {
		return "/mingame join - joins a game";

	}
	@Override
	public String permission() {
		return null;
	}

}
