package org.mcsg.minigame.commands;

import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.mcsg.minigame.Game;
import org.mcsg.minigame.GameManager;
import org.mcsg.minigame.Game.GameStatus;

public class Leave implements SubCommand{

	@Override
	public boolean onCommand(Player player, String[] args) {
		Game game = GameManager.getInstance().getGame(GameManager.getInstance().getPlayerGameId(player));
		if(game!= null){
			if (game.getStatus() != GameStatus.INGAME)
				game.removePlayer(player);
			else
				game.Killed(player, DamageCause.VOID);
		}
		return true;
	}


	@Override
	public String help(Player p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String permission() {
		// TODO Auto-generated method stub
		return null;
	}

}
