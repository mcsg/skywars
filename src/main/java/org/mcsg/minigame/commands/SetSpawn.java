package org.mcsg.minigame.commands;

import org.bukkit.entity.Player;
import org.mcsg.minigame.SettingsManager;

public class SetSpawn implements SubCommand{

	@Override
	public boolean onCommand(Player player, String[] args) {
		if(player.isOp()){
			SettingsManager.getInstance().setLobbySpawn(player.getLocation());
		}
		return true;
	}

	@Override
	public String help(Player p) {
		
		return null;
	}

	@Override
	public String permission() {
		return null;
	}

}
