package org.mcsg.minigame.commands;


import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.mcsg.minigame.GameManager;
import org.mcsg.minigame.MessageManager;
import org.mcsg.minigame.SkyWars;
import org.mcsg.minigame.MessageManager.PrefixType;
import org.mcsg.minigame.util.BlockData;
import org.mcsg.minigame.util.SchematicLoader;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;

public class CreateMap implements SubCommand{

	@Override
	public boolean onCommand(Player player, String[] args) {

		if(player.isOp()){
			WorldEditPlugin we = SkyWars.getPlugin().getWorldEdit();
			Selection sel = we.getSelection(player);

			if(sel == null){
				MessageManager.getInstance().sendMessage(PrefixType.ERROR, "Must make a worledit selection!", player);
				return false;
			}

			Location min = sel.getMinimumPoint();
			Location max = sel.getMaximumPoint();

			int xoff = -min.getBlockX();
			int zoff = -min.getBlockZ();

			World w = player.getWorld();

			ArrayList<BlockData>blocks = new ArrayList<BlockData>();
			for(int y = max.getBlockY(); y > min.getBlockY(); y--){
				for(int x = min.getBlockX(); x < max.getBlockX(); x++){
					for(int z = min.getBlockZ(); z < max.getBlockZ(); z++){
						Location l = new Location(w, x, y, z);
						//					 	System.out.println(l);
						Block bl = l.getBlock();
						if(bl.getTypeId() != 0){
							BlockData b = new BlockData(bl.getTypeId(), bl.getData(),x + xoff, y, z+zoff);
							blocks.add(b);
						}
					}
				}
			}

			SchematicLoader.getInstance().saveSchematic(blocks, args[0]);
			GameManager.getInstance().addSchematic(args[0]);

			player.sendMessage(ChatColor.GREEN+"Map created");
		}
		return true;
	}

	@Override
	public String help(Player p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String permission() {
		return null;
	}

}
