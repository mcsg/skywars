package org.mcsg.minigame.commands;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.mcsg.minigame.LobbySignManager;
import org.mcsg.minigame.MessageManager;
import org.mcsg.minigame.SettingsManager;
import org.mcsg.minigame.SkyWars;
import org.mcsg.minigame.MessageManager.PrefixType;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;

public class SetLobbyWall implements SubCommand{

	@Override
	public boolean onCommand(Player player, String[] args) {
		if(player.isOp()){
			
			WorldEditPlugin we = SkyWars.getPlugin().getWorldEdit();
			Selection sel = we.getSelection(player);

			if(sel == null){
				MessageManager.getInstance().sendMessage(PrefixType.ERROR, "Must make a worledit selection!", player);
				return false;
			}

			Location min = sel.getMinimumPoint();
			Location max = sel.getMaximumPoint();

			FileConfiguration s = SettingsManager.getInstance().getConfig();
			
			s.set("signs.world", min.getWorld().getName());
			s.set("signs.x1", max.getBlockX());
			s.set("signs.y1", max.getBlockY());
			s.set("signs.z1", max.getBlockZ());
			s.set("signs.x2", min.getBlockX());
			s.set("signs.y2", min.getBlockY());
			s.set("signs.z2", min.getBlockZ());
			
			SettingsManager.getInstance().saveConfig();
			LobbySignManager.getInstance().setup();
			
			player.sendMessage(ChatColor.GREEN+"Signs set");
		}
		
		return true;
	}

	@Override
	public String help(Player p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String permission() {
		// TODO Auto-generated method stub
		return null;
	}

}
