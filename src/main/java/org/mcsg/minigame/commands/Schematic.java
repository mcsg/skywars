package org.mcsg.minigame.commands;

import org.bukkit.entity.Player;
import org.mcsg.minigame.util.SchematicLoader;

public class Schematic implements SubCommand{

	@Override
	public boolean onCommand(Player player, String[] args) {
		if(player.isOp()){
			if(args.length == 1){
				if(args[0].equalsIgnoreCase("reload")){
					SchematicLoader.getInstance().clearCache();
				}
			} else if(args.length == 2){
				if(args[0].equalsIgnoreCase("remove")){
					SchematicLoader.getInstance().remove(args[1]);
				}

			}
		}
		return true;
	}

	@Override
	public String help(Player p) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String permission() {
		// TODO Auto-generated method stub
		return null;
	}

}
