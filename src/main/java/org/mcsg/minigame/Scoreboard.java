

package org.mcsg.minigame;

import java.util.Iterator;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;

public class Scoreboard
{
	private ScoreboardManager manager;
	private org.bukkit.scoreboard.Scoreboard scoreboard;
	private Objective objective;
	private Score alive;
	private Score broken;
	private Score placed;
	private Game game;

	public Scoreboard(Game g)
	{
		manager = SkyWars.getPlugin().getServer().getScoreboardManager();
		game = g;
		RenewScoreboard();
	}
	public void RenewScoreboard()
	{
		scoreboard = manager.getNewScoreboard();
		objective = scoreboard.registerNewObjective("PlayerAlive", "PlayersAlive"+game.getID());
		objective.setDisplayName(ChatColor.GREEN + "Game Stats");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		alive = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.AQUA+"Players Left:"));

		broken = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.LIGHT_PURPLE+"Blocks Broken:"));
		placed = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.LIGHT_PURPLE+"Blocks Placed:"));
	}
	public void Updatescoreboard()
	{
		alive.setScore(game.getPlayerCount());
		broken.setScore(game.getBlocksBroken());
		placed.setScore(game.getBlocksPlaced());

		Iterator<Player> iter = game.getPlayers().iterator();
		while(iter.hasNext()) {
			Player i = (Player)iter.next();
			Player playa = Bukkit.getPlayerExact(i.getName());
			try
			{
				playa.setScoreboard(scoreboard);
			}
			catch(Exception e) {
				game.removePlayer(playa);
			}
		}
	}
	public void Removescoreboards() {
		Iterator<Player> iter = game.getPlayers().iterator();
		while(iter.hasNext()) {
			Player i = (Player)iter.next();
			Player playa = Bukkit.getPlayerExact(i.getName());
			try
			{
				playa.setScoreboard(getManager().getNewScoreboard());
			}
			catch(Exception e) {
				game.removePlayer(playa);
			}
		}
	}

	public ScoreboardManager getManager() {
		return manager;
	}
}

