package org.mcsg.minigame.Tasks;

import org.bukkit.Bukkit;
import org.mcsg.minigame.Game;
import org.mcsg.minigame.SkyWars;

public class CountdownTask{
	
	private Game game;
	private int count = 45;

	public CountdownTask(Game g) {
		game = g;
	}

	public void start() {
		Bukkit.getScheduler().scheduleSyncDelayedTask(SkyWars.getPlugin(), new Runnable()
		{

			@Override
			public void run() {
				if(count <= 0){
					game.setAllEXP(0);
					game.startGame();
					return;
				}
				if (count <= 5) {
					game.msgFall("game.countdown2", "t-"+count);
				}
				else if(count % 10 == 0){
					game.msgFall("game.countdown", "t-"+count);
				}
				game.setAllEXP(count);

				count --;
				start();
			}

		}, 20L);
	}

	public int getTime(){
		return count;
	}


}
