package org.mcsg.minigame;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class DamageTracker
{
	static class Track {
		public Track(String k) {
			s = k; l =System.currentTimeMillis();
		}
		String s;
		Long l;
		
		public boolean expired() {
			return System.currentTimeMillis() > (l + 10000);
		}
	}
	
	
	//No direct access
	//<K=Player V=Hit by
	private static HashMap<String, Track> chips = new HashMap<String, Track>();
	
	public static void trackEDamageE(Player p, Player p1) {
		Player attacked = p;
		Player attacker = p1;
		if (attacked != null && attacker != null) {
			processAttack(attacked.getName(), attacker.getName());
			return;
		}
	}
	
	public static Player getLastDamager(Player p) {
		if (contains(p.getName())) {
			Track chip = chips.get(p.getName());
			
			if (!chip.expired()) {
				return Bukkit.getPlayerExact(chip.s);
			}
			else {
				destroyChip(p);
			}
		}
		return null;
	}
	
	public static void destroyChip(Player p) {
		removeKey(p.getName());
	}
	private static void removeKey(String s) {
		if (contains(s)) {
			chips.remove(s);
		}
	}
	private static void processAttack(String k, String v) {
		removeKey(k);
		chips.put(k, new Track(v));
	}
	private static boolean contains(String p) {
		return chips.containsKey(p);
	}
}
