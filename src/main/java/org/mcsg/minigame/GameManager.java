package org.mcsg.minigame;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.mcsg.minigame.Game.GameStatus;
import org.mcsg.minigame.util.VoidGenerator;

public class GameManager {

	static GameManager instance = new GameManager();
	private ArrayList < Game > games = new ArrayList < Game > ();
	private ArrayList < String > schematics = new ArrayList<String>();
	MessageManager msgmgr = MessageManager.getInstance();

	SkyWars plugin;

	private GameManager() {

	}

	public static GameManager getInstance() {
		return instance;
	}

	public void setup() {
		this.plugin = SkyWars.getPlugin();

		World w = Bukkit.createWorld(new WorldCreator(SkyWars.WORLD_NAME).generator(new VoidGenerator()));
		w.setAutoSave(false);
		w.setKeepSpawnInMemory(false);
		w.setWeatherDuration(0);
		w.setDifficulty(Difficulty.PEACEFUL);


		File folder = new File(getPlugin().getDataFolder(), "schematics");
		folder.mkdirs();

		String[] files = folder.list();
		if(files != null){
			for(String schem:files){
				addSchematic(schem);
			}
		}

	}

	public void addSchematic(String schem){
		schematics.add(schem);
		System.out.println("Adding schem "+schem);
	}

	public Plugin getPlugin() {
		return plugin;
	}


	public int getPlayerGameId(Player p) {
		for (Game g: games) {
			if (g.containsPlayer(p))
				return g.getID();
		}
		return -1;
	}

	public boolean isPlayerActive(Player player) {
		//We do not track dead players
		//Sooo we just ask if the game knows they exist
		return getPlayerGameId(player) != -1;
	}
	public boolean inGame(Player player) {
		//We do not track dead players
		//Sooo we just ask if the game knows they exist
		return getPlayerGameId(player) != -1;
	}
	public boolean isPlayerInactive(Player player) {
		//We do not track dead players
		//Soooo we just ask if the game knows they exist
		return getPlayerGameId(player) == -1;
	}
	public int getGameCount() {
		return games.size();
	}

	public int getTotalPlayers(){
		int t = 0;
		for(Game g:getGames()){
			t += g.getPlayerCount();
		}
		return t;
	}

	public Game getGame(int a) {
		//int t = gamemap.get(a);
		for (Game g: games) {
			if (g.getID() == a) {
				return g;
			}
		}
		return null;
	}
	public void leaveGame(Player p) {
		Game g = getGame(getPlayerGameId(p));
		if (g != null)
			g.playerLeave(p);
	}
	public ArrayList < Game > getGames() {
		return games;
	}

	public GameStatus getGameStatus(int a) {
		for (Game g: games) {
			if (g.getID() == a) {
				return g.getStatus();
			}
		}
		return null;
	}


	//TODO: Actually make this countdown correctly
	public void startGame(int a) {
		//getGame(a).countdown(10);
	}

	public void addPlayer(Player p){
		//createNewGame();
		//createNewGame();

		if(games.size() == 0){
			createNewGame();
		}
		Game g = games.get(games.size() - 1);
		if(g.spawnsFull() || g.getStatus() != GameStatus.WAITING){
			createNewGame().addPlayer(p);
		}
		else{
			g.addPlayer(p);
		}
		//createNewGame();

	}

	public Game createNewGame(){
		int id = Game.getGameID();
		Map map = getNextMap(id);

		Game g1 = new Game(map, id);
		games.add(g1);
		return g1;
	}

	public void removeGame(Game g) {
		if (games.contains(g)){
			games.remove(g);
		}
	}

	private Map getNextMap(int id){
		Random r = new Random();
		System.out.println(schematics.size());
		String schem = schematics.get(r.nextInt(schematics.size()));

		Map m = new Map(schem, (id * 500) + 5000000, 0);

		return m;
	}

	public void endGame(Game g){
		games.remove(g);
	}
}