package org.mcsg.minigame;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class InventoryManager
{
    private static HashMap<String, ItemStack[]> Inventories = new HashMap<String, ItemStack[]>();
    private static HashMap<String, ItemStack[]> Equipment = new HashMap<String, ItemStack[]>();

    public static void store(Player player)
    {
        if (Inventories.containsKey(player.getName()) || Equipment.containsKey(player.getName())) {
            restore(player);
        }
        Inventories.put(player.getName(), player.getInventory().getContents());
        Equipment.put(player.getName(), player.getEquipment().getArmorContents());

        clearInventory(player);
    }
    public static void restore(Player player)
    {
        if (Inventories.containsKey(player.getName()) || Equipment.containsKey(player.getName())) {
        	clearInventory(player);
            player.getInventory().setContents(Inventories.get(player.getName()));
            player.getEquipment().setArmorContents(Equipment.get(player.getName()));

            Inventories.remove(player.getName());
            Equipment.remove(player.getName());
        }
    }
	

    @SuppressWarnings("deprecation")
        public static void clearInventory(Player player) {
                player.getInventory().setArmorContents(new ItemStack[4]);
                player.getInventory().setContents(new ItemStack[]{});
               
                //Make sure, that shit, is fucking clear
                player.getInventory().setHelmet(new ItemStack(Material.AIR));
                player.getInventory().setLeggings(new ItemStack(Material.AIR));
                player.getInventory().setBoots(new ItemStack(Material.AIR));
                player.getInventory().setChestplate(new ItemStack(Material.AIR));
                player.updateInventory();
        }


}

