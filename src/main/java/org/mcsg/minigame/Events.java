package org.mcsg.minigame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;


import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Fish;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;
import org.mcsg.minigame.Game.GameStatus;
import org.mcsg.minigame.util.ChestRatioStorage;
import org.mcsg.minigame.util.FireworkEffectPlayer;


public class Events implements Listener
{
	public static void setup()
	{
		SkyWars.getPlugin().getServer().getPluginManager().registerEvents(new Events(), SkyWars.getPlugin());
	}


	@EventHandler
	public void onBlockBreak(BlockBreakEvent e)
	{
		Player p = e.getPlayer();
		if (p != null) {
			Game game = GameManager.getInstance().getGame(GameManager.getInstance().getPlayerGameId(p));
			if (game != null) {
				if (game.getStatus() != GameStatus.INGAME || e.getBlock().getType() == Material.CHEST)
					e.setCancelled(true);
				else
					game.addBlockBroken();
			}
		}
	}
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent e)
	{
		Player p = e.getPlayer();
		if (p != null) {
			Game game = GameManager.getInstance().getGame(GameManager.getInstance().getPlayerGameId(p));
			if (game != null) {
				if (game.getStatus() != GameStatus.INGAME || e.getBlock().getType() == Material.CHEST)
					e.setCancelled(true);
				else
					game.addBlockPlaced();
			}
		}
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e)
	{
		Player p = e.getEntity();
		e.setDeathMessage(null);
		if (p != null) {
			Game game = GameManager.getInstance().getGame(GameManager.getInstance().getPlayerGameId(p));
			if (game != null) {
				if (game.getStatus() != GameStatus.INGAME)
					game.removePlayer(p);
				else if(p.getKiller() != null)
					game.Killed(p, p.getKiller());
				else
					game.Killed(p, DamageCause.VOID);
			}
		}
	}
	
	@EventHandler
	public void chunkLoad(ChunkLoadEvent e){
		if(e.getWorld().getName().equals(SkyWars.WORLD_NAME))
			SkyWars.chunks.add(e.getChunk());
	}
	
	@EventHandler
	public void chunkUnLoad(ChunkUnloadEvent e){
		if(e.getWorld().getName().equals(SkyWars.WORLD_NAME))
			SkyWars.chunks.remove(e.getChunk());
	}
	
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e)
	{
		Player p = e.getPlayer();
		if (p != null) {
			Game game = GameManager.getInstance().getGame(GameManager.getInstance().getPlayerGameId(p));
			if (game != null) {
				if (game.getStatus() != GameStatus.INGAME)
					game.removePlayer(p);
				else
					game.Killed(p, DamageCause.VOID);
			}
		}
	}
	@EventHandler (priority = EventPriority.LOW)
	public void onJoin(PlayerJoinEvent e)
	{
		//If they left during a game, and the inventory wasnt restored but still in memory
		//We can just do this ;o
		if(e.getPlayer().getWorld().getName().equals(SkyWars.WORLD_NAME)){
			e.getPlayer().teleport(SettingsManager.getInstance().getLobbySpawn());
		}
		InventoryManager.restore(e.getPlayer());
	}
	@EventHandler
	public void onPortalEnter(PlayerPortalEvent e)
	{
		if(e.getCause() == TeleportCause.END_PORTAL){
			e.setCancelled(true);
			GameManager.getInstance().addPlayer(e.getPlayer());
		}
	}
	@EventHandler
	public void onEntityDamage(EntityDamageEvent e)
	{
		if (e.getCause() != DamageCause.ENTITY_ATTACK)
		{
			if (e.getEntity() instanceof Player)
			{
				Player p = (Player)e.getEntity();
				if (p != null) {
					Game game = GameManager.getInstance().getGame(GameManager.getInstance().getPlayerGameId(p));
					if(game != null){
						if(game.isGracePeriod() && e.getCause() != DamageCause.VOID){
							e.setCancelled(true);
						}
						else if (game != null && (p.getHealth() - e.getDamage()) <= 0) {
							game.Killed(p, e.getCause());
						}
					}
				}
			}
		}
	}
	@EventHandler
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e) {
		if (e.getEntity() instanceof Player) {
			Player attacked = (Player)e.getEntity();
			if (GameManager.getInstance().inGame(attacked))
			{
				Player attacker = null;
				if (e.getDamager() instanceof Player) {
					attacker = (Player)e.getDamager();
				}
				else if (e.getDamager() instanceof Projectile) {
					Projectile p = (Projectile)e.getDamager();
					if (p.getShooter() instanceof Player && (attacker == null)) {
						attacker = (Player)p.getShooter();
					}
				}
				else if (e.getDamager() instanceof Fish) {
					Fish f = (Fish)e.getDamager();
					if (f.getShooter() instanceof Player && (attacker == null)) {
						attacker = (Player)f.getShooter();
					}
				}
				if (attacker != null) {
					DamageTracker.trackEDamageE(attacked, attacker);
				}
			}
		}
	}
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
		InventoryManager.restore(e.getPlayer());
		e.setRespawnLocation(SettingsManager.getInstance().getLobbySpawn());
	}

    @EventHandler
    public void onPreprocess(PlayerCommandPreprocessEvent e) {
            Player p = e.getPlayer();
            if (p != null) {
            	if (GameManager.getInstance().inGame(p)) {
            		boolean allowed = p.hasPermission("skywars.bypass");
	            	if (!allowed) {
	            		if (!e.getMessage().toLowerCase().startsWith("/sw")) {
	            			e.setCancelled(true);
	            			return;
	            		}
	            	}
            	}
            }
    }

	private static final ArrayList<Block>opened_chest = new ArrayList<Block>();

	@EventHandler
	public void ChestListener(PlayerInteractEvent e){
		if(!(e.getAction()==Action.RIGHT_CLICK_BLOCK)) return;
		Block b = e.getClickedBlock();
		Player p = e.getPlayer();
		if(b.getState() instanceof Chest){
			if(e.getPlayer().getWorld().getName().equals(SkyWars.WORLD_NAME)){
				if(!opened_chest.contains(b)){
					boolean lucky = (new Random().nextInt(100) >= 99 ? true : false);

					if (p.hasPermission("skywars.lucky") && !lucky)
						lucky = (new Random().nextInt(100) >= 95 ? true : false);
					if (SkyWars.isDev(p.getName()) && !lucky)
						lucky = (new Random().nextInt(100) >= 90 ? true : false);

					if (lucky) {
						GameManager.getInstance().getGame(GameManager.getInstance().getPlayerGameId(p)).msgFall("game.luckyguy", "player-"+p.getName());
						FireworkEffectPlayer fep = new FireworkEffectPlayer();
						Location loc = b.getLocation().clone();
						loc.setY(loc.getY()+1);
						try { fep.playFirework(loc.getWorld(), loc, FireworkEffectPlayer.getRandomEffect()); } catch(Exception x) {}
					}

					ArrayList<ItemStack>items = ChestRatioStorage.getInstance().getItems((lucky ? ChestRatioStorage.ChestLevel.OP : ChestRatioStorage.getInstance().getChestLevel(b.getLocation())));
					Chest c = (Chest)b.getState();
					c.getInventory().clear();
					for(ItemStack i:items){
						c.getInventory().addItem(i);
					}
					opened_chest.add(b);
					c.update(true);
				}
			}
		}
	}
}
